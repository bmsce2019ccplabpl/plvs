#include <stdio.h>
int main()
{
	int sum=0,i,n;
    n=input();
    i=n;
    sum=compute(n,i,sum);
    output(n,sum);
}

int input()
{
    int a;
    printf("Enter the number\n");
    scanf("%d",&a);
    return a;
}

int compute(int n, int i,int sum)
{
    int r;
    while(i>0)
    {
        r=n%10;
        sum=sum+r;
        n=n/10;
        i--;
    }
    return sum;
}

void output(int n, int sum)
{
    printf("The sum of %d digits is %d\n",n,sum);
}