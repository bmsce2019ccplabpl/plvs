#include<stdio.h>
int input()
{
    int a;
    scanf("%d",&a);
    return a;
}
void swap(int *a, int *b)
{
    int temp=*a;
    *a=*b;
    *b=temp;
}
void display(a,b)
{
    printf("The values after swapping are\n");
    printf("A=%d \nB=%d\n",a,b);
}
int main()
{
    printf("Enter any two numbers A and B\n");
    int p=input();
    int q=input();
    swap(&p,&q);
    display(p,q);
    return 0;
}