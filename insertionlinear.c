#include<stdio.h>

int main()
{
    int n,pos,ele;
    printf("Enter the number of elements in the array\n");
    scanf("%d",&n);
    int a[n+1];
    printf("Enter the array elements\n");
    for(int i=0;i<n;i++)
        scanf("%d",&a[i]);
    printf("Enter the new element and the index of the new element to be inserted\n");
    scanf("%d%d",&ele,&pos);
    if(pos<=n)
    {
        for(int j=n-1;j>=pos;j--)
        {
            a[j+1]=a[j];
        }
        a[pos]=ele, n=n+1;
        printf("The array after inserting the new number is\n");
        for(int j=0;j<n;j++)
            printf("%d\n",a[j]);
    }
    else
        printf("Invalid position\n");
    return 0;
}
